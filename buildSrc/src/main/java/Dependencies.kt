package dependencies

object Dependencies {

    private const val path = "../codeQuality/"
    const val common = "${path}common.gradle"

    object ClassPaths {
        const val gradleClasspath = "com.android.tools.build:gradle:${Version.gradle}"
        const val kotlinGradlePluginClasspath = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Version.kotlin}"
        const val jacoco = "com.vanniktech:gradle-android-junit-jacoco-plugin:${Version.jacoco}"
        const val navigation = "androidx.navigation:navigation-safe-args-gradle-plugin:${Version.navigation}"
        const val crashlytics = "com.google.firebase:firebase-crashlytics-gradle:${Version.crashlyticsGradlePlugin}"
    }

    object Plugins {
        const val JACOCO = "com.vanniktech.android.junit.jacoco"
    }

    object Lint {
        const val detekt = "io.gitlab.arturbosch.detekt:detekt-cli:${Version.detekt}"
        const val ktLint = "com.pinterest:ktlint:${Version.ktLint}"
    }

    object Module {
        const val data = ":data"
        const val cache = ":cache"
        const val remote = ":remote"
        const val domain = ":domain"
        const val presentation = ":presentation"
    }

    object Firebase {
        const val firebaseCore = "com.google.firebase:firebase-core:${Version.firebase}"
        const val firebaseAnalytics = "com.google.firebase:firebase-analytics-ktx:${Version.firebase}"
        const val crashlytics = "com.google.firebase:firebase-crashlytics:${Version.crashlytics}"
    }

    object Facebook {
        const val stetho = "com.facebook.stetho:stetho:${Version.stetho}"
        const val stethoNetwork = "com.facebook.stetho:stetho-okhttp3:${Version.stetho}"
    }

    object Coil {
        const val coil = "io.coil-kt:coil:${Version.coil}"
    }

    object Gson {
        const val gson = "com.google.code.gson:gson:${Version.gson}"
    }

    object Kotlin {
        const val kotlin_stdlib = "org.jetbrains.kotlin:kotlin-stdlib:${Version.kotlin}"
    }

    object OkHttp3 {
        const val loggingInterceptor = "com.squareup.okhttp3:logging-interceptor:${Version.okHttp}"
        const val okHttp3 = "com.squareup.okhttp3:okhttp:${Version.okHttp}"
    }

    object Retrofit2 {
        const val adapterRxjava = "com.squareup.retrofit2:adapter-rxjava3:${Version.retrofit}"
        const val converterGson = "com.squareup.retrofit2:converter-gson:${Version.retrofit}"
        const val retrofit = "com.squareup.retrofit2:retrofit:${Version.retrofit}"
    }

    object AndroidX {
        const val core = "androidx.core:core:${Version.androidx}"
        const val constraintLayout =
            "androidx.constraintlayout:constraintlayout:${Version.constraintLayout}"
        const val swipeRefreshLayout =
            "androidx.swiperefreshlayout:swiperefreshlayout:${Version.swipeRefreshLayout}"
        const val materialDesign = "com.google.android.material:material:${Version.materialDesign}"
        const val recyclerView = "androidx.recyclerview:recyclerview:${Version.recyclerView}"
        const val appcompat = "androidx.appcompat:appcompat:${Version.appCompat}"
        const val fragmentExtension = "androidx.fragment:fragment-ktx:${Version.fragmentKtx}"
        const val navigation = "androidx.navigation:navigation-ui-ktx:${Version.navigation}"
        const val navigationKtx = "androidx.navigation:navigation-fragment-ktx:${Version.navigation}"
    }

    object Lifecycle {
        const val annotation_compiler = "androidx.lifecycle:lifecycle-compiler:${Version.lifecycleVersion}"
        const val viewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Version.lifecycleVersion}"
        const val liveData = "androidx.lifecycle:lifecycle-livedata-ktx:${Version.lifecycleVersion}"
    }

    object Room {
        const val runtime = "androidx.room:room-runtime:${Version.room}"
        const val annotationProcessor = "androidx.room:room-compiler:${Version.room}"
        const val roomRx = "androidx.room:room-rxjava3:${Version.room}"
    }

    object RxJava {
        const val rxJava = "io.reactivex.rxjava3:rxjava:${Version.rxJava}"
        const val rxAndroid = "io.reactivex.rxjava3:rxandroid:${Version.rxAndroid}"
    }

    object Paging {
        const val runtime = "androidx.paging:paging-runtime:${Version.paging}"
        const val common = "androidx.paging:paging-common:${Version.paging}"
        const val pagingRx = "androidx.paging:paging-rxjava3:${Version.paging}"
    }

    object Koin {
        const val core = "io.insert-koin:koin-core:${Version.koin}"
        const val android = "io.insert-koin:koin-android:${Version.koin}"
    }

    object ViewBinding {
        const val bindingDelegate = "com.github.kirich1409:viewbindingpropertydelegate:${Version.viewBindingDelegate}"
    }

    object Test {
        const val test_junit = "junit:junit:${Version.junit}"
        const val android_test_espresso_core =
            "androidx.test.espresso:espresso-core:${Version.espresso}"
        const val android_test_room = "android.arch.persistence.room:testing:${Version.room}"
        const val testing_core_testing = "android.arch.core:core-testing:${Version.lifecycle}"
        const val android_test_rules = "androidx.test:rules:${Version.rules}"
        const val android_test_runner = "androidx.test:runner:${Version.runner}"
        const val mockito = "org.mockito:mockito-core:${Version.mockito}"
        const val assert_j = "org.assertj:assertj-core:${Version.assertJVersion}"
        const val robolectric = "org.robolectric:robolectric:${Version.robolectric}"
    }

    const val javax = "javax.inject:javax.inject:${Version.javaxInject}"
}
