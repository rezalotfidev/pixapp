package dependencies

object Version {

    // android configuration
    const val compileSdk = 33
    const val minSdk = 23
    const val targetSdk = 33
    const val versionCode = 1
    const val versionName = "1.0"

    // Libraries
    const val recyclerView = "1.2.0"
    const val androidx = "1.7.0"
    const val constraintLayout = "1.1.3"
    const val swipeRefreshLayout = "1.1.0"
    const val appCompat = "1.4.0"
    const val fragmentKtx = "1.4.0"
    const val navigation = "2.3.5"
    const val viewBindingDelegate = "1.5.6"
    const val assertJVersion = "3.2.0"
    const val materialDesign = "1.4.0"
    const val mockito = "4.1.0"
    const val koin = "3.3.2"
    const val lifecycleVersion = "2.4.0"
    const val javaxInject = "1"
    const val jacoco = "0.16.0"
    const val coil = "1.4.0"
    const val gradle = "7.0.3"
    const val kotlin = "1.6.0"
    const val ktLint = "0.43.2"
    const val espresso = "3.4.0"
    const val gson = "2.8.5"
    const val junit = "4.10"
    const val lifecycle = "2.4.0"
    const val retrofit = "2.9.0"
    const val okHttp = "4.9.3"
    const val robolectric = "4.7.3"
    const val room = "2.3.0"
    const val rules = "1.4.0"
    const val runner = "1.4.0"
    const val rxJava = "3.1.3"
    const val rxAndroid = "3.0.0"
    const val paging = "3.1.1"
    const val stetho = "1.5.0"
    const val crashlytics = "18.2.4"
    const val crashlyticsGradlePlugin = "2.8.1"
    const val firebase = "20.0.0"

    // plugins versions
    const val detekt = "1.18.1"
}
