package com.reza.pixapp.app.di

import com.reza.pixapp.app.di.modules.appModule
import com.reza.pixapp.cache.di.cacheModule
import com.reza.pixapp.data.di.dataModule
import com.reza.pixapp.domain.di.domainModule
import com.reza.pixapp.remote.di.remoteModule


val appComponents = listOf(appModule, domainModule, dataModule, cacheModule, remoteModule)
