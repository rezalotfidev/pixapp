package com.reza.pixapp.app.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.reza.pixapp.app.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }
}