package com.reza.pixapp.app.ui.photos

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.PagingData
import by.kirich1409.viewbindingdelegate.viewBinding
import com.reza.pixapp.app.R
import com.reza.pixapp.app.databinding.FragmentPhotosBinding
import com.reza.pixapp.app.utils.extension.gone
import com.reza.pixapp.app.utils.extension.visible
import com.reza.pixapp.presentation.model.PhotoUiModel
import com.reza.pixapp.presentation.viewmodel.photos.PhotosState
import com.reza.pixapp.presentation.viewmodel.photos.PhotosViewModel
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel


class PhotosFragment : Fragment(R.layout.fragment_photos) {

    private val binding: FragmentPhotosBinding by viewBinding()
    private val viewModel: PhotosViewModel by viewModel()
    private val photoAdapter by lazy { PhotoAdapter() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
        initObservables()
        viewModel.fetchPhotosList()
    }

    private fun initViews() {
        initRecyclerView()
        initRefreshLayout()
        hideLoading()
    }

    private fun initObservables() {
        viewModel.stateObservable.observe(viewLifecycleOwner) {
            when (it) {
                is PhotosState.Loading -> showLoading()
                is PhotosState.Error -> showErrorLayout(it.message)
                is PhotosState.PhotoListSuccess -> showListUI(it.listOfPhotoModels)
            }
        }
    }

    private fun initRecyclerView() {
        photoAdapter.onClickItem = {
            findNavController().navigate(
                PhotosFragmentDirections.actionPhotosFragmentToPhotoDetailFragment(it!!.id)
            )
        }
        binding.listRecyclerView.adapter = photoAdapter
    }

    private fun initRefreshLayout() {
        binding.refreshLayout.setOnRefreshListener {
            viewModel.fetchPhotosList()
        }
    }

    private fun showListUI(data: PagingData<PhotoUiModel>) =
        lifecycleScope.launch {
            hideLoading()
            binding.layoutError.rootLayout.gone()
            binding.listRecyclerView.visible()
            photoAdapter.submitData(data)
        }


    private fun showErrorLayout(errorMessage: String) {
        hideLoading()
        binding.listRecyclerView.gone()
        binding.layoutError.txtErrorMessage.text = errorMessage
        binding.layoutError.rootLayout.visible()
    }

    private fun showLoading() {
        binding.refreshLayout.isEnabled = true
        binding.refreshLayout.isRefreshing = true
        binding.listRecyclerView.gone()
        binding.layoutError.rootLayout.gone()
    }

    private fun hideLoading() {
        binding.refreshLayout.isRefreshing = false
    }

}
