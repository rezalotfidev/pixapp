package com.reza.pixapp.app.ui.photos

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.reza.pixapp.app.R
import com.reza.pixapp.app.databinding.RecyclerItemPhotoBinding
import com.reza.pixapp.presentation.model.PhotoUiModel

class PhotoAdapter : PagingDataAdapter<PhotoUiModel, PhotoAdapter.ViewHolder>(PhotosDiffCallBack()) {

    var onClickItem: ((PhotoUiModel?) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        RecyclerItemPhotoBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(private val binding: RecyclerItemPhotoBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener { onClickItem?.invoke(getItem(layoutPosition)) }
        }

        fun bind(item: PhotoUiModel?) {
            binding.apply {
                imageView.load(item?.thumbnailUrl)
                txtTitle.text = item?.title
                txtAlbumId.text = root.context.getString(R.string.album_id).plus(item?.albumId)
            }
        }

    }


    class PhotosDiffCallBack : DiffUtil.ItemCallback<PhotoUiModel>() {
        override fun areItemsTheSame(oldItem: PhotoUiModel, newItem: PhotoUiModel): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: PhotoUiModel, newItem: PhotoUiModel): Boolean {
            return oldItem == newItem
        }
    }


}