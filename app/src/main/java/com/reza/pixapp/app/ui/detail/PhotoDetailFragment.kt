package com.reza.pixapp.app.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import by.kirich1409.viewbindingdelegate.viewBinding
import coil.load
import com.reza.pixapp.app.R
import com.reza.pixapp.app.databinding.FragmentPhotoDetailBinding
import com.reza.pixapp.app.utils.extension.gone
import com.reza.pixapp.app.utils.extension.visible
import com.reza.pixapp.presentation.model.PhotoUiModel
import com.reza.pixapp.presentation.viewmodel.details.PhotoDetailState
import com.reza.pixapp.presentation.viewmodel.details.PhotoDetailViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class PhotoDetailFragment : Fragment(R.layout.fragment_photo_detail) {

    private val binding: FragmentPhotoDetailBinding by viewBinding()
    private val viewModel: PhotoDetailViewModel by viewModel()
    private val args: PhotoDetailFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        initObservables()
        viewModel.fetchPhotoDetails(args.id)
    }

    private fun initViews() {
        binding.refreshLayout.setOnRefreshListener {
            viewModel.fetchPhotoDetails(args.id)
        }
    }

    private fun initObservables() {
        viewModel.stateObservable.observe(viewLifecycleOwner) {
            when (it) {
                is PhotoDetailState.Loading -> showLoading()
                is PhotoDetailState.Error -> showErrorLayout(it.message)
                is PhotoDetailState.PhotoListSuccess -> showData(it.photoUiModel)
            }
        }
    }

    private fun showData(model: PhotoUiModel) {
        hideLoading()
        binding.imageView.load(model.url)
        binding.txtTitle.text = model.title
        binding.txtURL.text = model.url
        binding.txtAlbumId.text = getString(R.string.album_id).plus(model.albumId.toString())
        binding.layoutError.rootLayout.gone()
        binding.layoutData.visible()
    }

    private fun showErrorLayout(errorMessage: String) {
        hideLoading()
        binding.layoutData.gone()
        binding.layoutError.txtErrorMessage.text = errorMessage
        binding.layoutError.rootLayout.visible()
    }

    private fun showLoading() {
        binding.refreshLayout.isEnabled = true
        binding.refreshLayout.isRefreshing = true
        binding.layoutData.gone()
        binding.layoutError.rootLayout.gone()
    }

    private fun hideLoading() {
        binding.refreshLayout.isRefreshing = false
        binding.refreshLayout.isEnabled = false
    }

}