package com.reza.pixapp.app.di.modules

import com.reza.pixapp.app.BuildConfig
import com.reza.pixapp.app.utils.UiThread
import com.reza.pixapp.domain.executor.PostExecutionThread
import com.reza.pixapp.presentation.mapper.PhotoUiMapper
import com.reza.pixapp.presentation.viewmodel.details.PhotoDetailViewModel
import com.reza.pixapp.presentation.viewmodel.photos.PhotosViewModel
import com.reza.pixapp.remote.di.IS_DEBUG
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.StringQualifier
import org.koin.dsl.module

val appModule = module {

    // PhotosViewModel
    viewModel {
        PhotosViewModel(get(),get())
    }

    // PhotoDetailViewModel
    viewModel {
        PhotoDetailViewModel(get(),get())
    }

    // PostExecutionThread
    single<PostExecutionThread> {
        UiThread()
    }

    // Debug Variant status
    single(StringQualifier(IS_DEBUG)) {
        BuildConfig.DEBUG
    }

    // PhotoUiMapper
    single {
        PhotoUiMapper()
    }
}