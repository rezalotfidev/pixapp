package com.reza.pixapp.app

import android.app.Application

import com.facebook.stetho.Stetho
import com.reza.pixapp.app.di.appComponents
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MainApp : Application() {


    override fun onCreate() {
        super.onCreate()

        setupDI()
        setupStetho()
    }

    private fun setupDI(){
        startKoin {
            androidLogger()
            androidContext(this@MainApp)
            modules(appComponents)
        }
    }

    private fun setupStetho() {
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }
    }
}
