package com.reza.pixapp.remote

import com.reza.pixapp.remote.api.PhotoApiService
import com.reza.pixapp.remote.factory.RemotePhotoFactory
import com.reza.pixapp.remote.mappers.PhotosNetMapper
import com.reza.pixapp.remote.models.PhotoNetModel
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PhotosRemoteImpTest {

    @Mock
    private lateinit var photoApiService: PhotoApiService

    private lateinit var photosNetMapper: PhotosNetMapper

    private lateinit var photoRemoteImp: PhotosRemoteImp

    @Before
    fun setUp() {
        photosNetMapper = PhotosNetMapper()
        photoRemoteImp =
            PhotosRemoteImp(photoApiService, photosNetMapper)
    }

    @Test
    fun getPhotos_Completes() {
        // Arrange
        stubPhotos(Single.just(RemotePhotoFactory.generateListOfPhotoModel(6)))

        // Act
        val testObserver = photoRemoteImp.getPhotosList().test()

        // Assert
        testObserver.assertComplete()
    }

    @Test
    fun getPhotos_returnsData() {
        // Arrange
        stubPhotos(Single.just(RemotePhotoFactory.generateListOfPhotoModel(6)))

        // Act
        val testObserver = photoRemoteImp.getPhotosList().test()

        // Assert
        assert(testObserver.values()[0].size == 6)
    }


    @Test
    fun getPhotoDetail_Completes(){
        // Arrange
        stubPhotoDetail(Single.just(RemotePhotoFactory.generatePhotoNetModel()))

        // Act
        val testObserver = photoRemoteImp.getPhotoDetails(0).test()

        // Assert
        testObserver.assertComplete()
    }

    /**
     * Stub helpers
     */

    private fun stubPhotos(single: Single<List<PhotoNetModel>>) {
        `when`(photoApiService.getPhotosList()).thenReturn(
            single
        )
    }

    private fun stubPhotoDetail(single: Single<PhotoNetModel>){
        `when`(photoApiService.getPhotoDetails(0)).thenReturn(
            single
        )
    }
}
