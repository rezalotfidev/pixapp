package com.reza.pixapp.remote.factory

import com.reza.pixapp.remote.models.PhotoNetModel

object RemotePhotoFactory {

    fun generateListOfPhotoModel(size: Int): MutableList<PhotoNetModel> {
        val listOfPhotoModels = mutableListOf<PhotoNetModel>()
        repeat(size) {
            listOfPhotoModels.add(generatePhotoNetModel())
        }
        return listOfPhotoModels
    }

    fun generatePhotoNetModel(): PhotoNetModel {
        return PhotoNetModel(
            id = DataFactory.getRandomInt(),
            albumId = DataFactory.getRandomInt(),
            title = DataFactory.getRandomString(),
            url = DataFactory.getRandomString(),
            thumbnailUrl = DataFactory.getRandomString()
        )
    }

}
