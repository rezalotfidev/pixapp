package com.reza.pixapp.remote.mappers

import com.reza.pixapp.data.models.PhotoDataModel
import com.reza.pixapp.remote.factory.RemotePhotoFactory
import com.reza.pixapp.remote.models.PhotoNetModel
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PhotosNetMapperTest {

    private lateinit var photosNetMapper: PhotosNetMapper

    @Before
    fun setUp() {
        photosNetMapper = PhotosNetMapper()
    }

    @Test
    fun mapFromModel() {
        // Arrange
        val photoNetModel = RemotePhotoFactory.generatePhotoNetModel()

        // Act
        val photoDataModel = photosNetMapper.mapFromNet(photoNetModel)

        // Assert
        assertMapPhotoDataEquals(photoNetModel, photoDataModel)
    }

    /**
     * Helper Methods
     */
    private fun assertMapPhotoDataEquals(photoNetModel: PhotoNetModel, photoDataModel: PhotoDataModel) {
        assertEquals(photoDataModel.id, photoNetModel.id)
        assertEquals(photoDataModel.albumId, photoNetModel.albumId)
        assertEquals(photoDataModel.title, photoNetModel.title)
        assertEquals(photoDataModel.url, photoNetModel.url)
        assertEquals(photoDataModel.thumbnailUrl, photoNetModel.thumbnailUrl)
    }
}
