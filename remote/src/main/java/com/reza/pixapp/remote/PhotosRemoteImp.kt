package com.reza.pixapp.remote

import com.reza.pixapp.data.repository.PhotosRemote
import com.reza.pixapp.data.models.PhotoDataModel
import com.reza.pixapp.remote.api.PhotoApiService
import com.reza.pixapp.remote.mappers.PhotosNetMapper
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class PhotosRemoteImp (
    private val photoApiService: PhotoApiService,
    private val photosNetMapper: PhotosNetMapper
) : PhotosRemote {

    override fun getPhotosList(): Single<List<PhotoDataModel>> {
        return photoApiService.getPhotosList().map { photoNetModelsList ->
            photoNetModelsList.map {
                photosNetMapper.mapFromNet(it)
            }
        }
    }

    override fun getPhotoDetails(id: Int): Single<PhotoDataModel> {
        return photoApiService.getPhotoDetails(id).map {
            photosNetMapper.mapFromNet(it)
        }
    }
}
