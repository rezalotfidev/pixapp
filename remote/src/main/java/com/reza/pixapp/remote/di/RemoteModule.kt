package com.reza.pixapp.remote.di

import com.reza.pixapp.data.repository.PhotosRemote
import com.reza.pixapp.remote.PhotosRemoteImp
import com.reza.pixapp.remote.api.PhotoApiServiceFactory
import com.reza.pixapp.remote.utils.NetConstants
import com.reza.pixapp.remote.mappers.PhotosNetMapper
import org.koin.core.qualifier.StringQualifier
import org.koin.dsl.module

const val IS_DEBUG = "is_debug"
val remoteModule = module {

    // PhotoApiService
    single {
        PhotoApiServiceFactory.create(
            get(StringQualifier(IS_DEBUG)),
            NetConstants.BASE_URL
        )
    }

    // PhotosNetMapper
    single {
        PhotosNetMapper()
    }

    // PhotosRemote
    single<PhotosRemote> {
        PhotosRemoteImp(get(),get())
    }
}