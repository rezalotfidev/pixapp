package com.reza.pixapp.remote.models

data class PhotoNetModel(
    val albumId: Int,
    val id: Int,
    val thumbnailUrl: String,
    val title: String,
    val url: String
)