package com.reza.pixapp.remote.api

import com.reza.pixapp.remote.models.PhotoNetModel
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.PUT

interface PhotoApiService {

    @GET("photos/")
    fun getPhotosList(): Single<List<PhotoNetModel>>

    @PUT("photos/{id}")
    fun getPhotoDetails(id: Int): Single<PhotoNetModel>
}
