package com.reza.pixapp.remote.mappers

import com.reza.pixapp.data.models.PhotoDataModel
import com.reza.pixapp.remote.models.PhotoNetModel

class PhotosNetMapper {
    fun mapFromNet(model: PhotoNetModel): PhotoDataModel {
        return PhotoDataModel(
            id = model.id,
            albumId = model.albumId,
            thumbnailUrl = model.thumbnailUrl,
            title = model.title,
            url = model.url,
        )
    }
}
