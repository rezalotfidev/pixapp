package com.reza.pixapp.domain.usecase

import com.reza.pixapp.domain.executor.PostExecutionThread
import com.reza.pixapp.domain.executor.ThreadExecutor
import com.reza.pixapp.domain.factory.PhotoDomainModelFactory
import com.reza.pixapp.domain.model.PhotoDomainModel
import com.reza.pixapp.domain.repositories.PhotosRepository
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetPhotoListUseCaseTest {

    @Mock
    lateinit var photosRepository: PhotosRepository

    @Mock
    lateinit var threadExecutor: ThreadExecutor

    @Mock
    lateinit var postExecutionThread: PostExecutionThread

    lateinit var getPhotoListUseCase: GetPhotoListUseCase

    @Before
    fun setUp() {
        getPhotoListUseCase =
            GetPhotoListUseCase(photosRepository, threadExecutor, postExecutionThread)
    }

    @Test
    fun buildUseCaseObservable_call_repository() {
        // Arrange
        // No Arrangement for this test case

        // Act
        getPhotoListUseCase.buildUseCaseObservable()

        // Assert
        verify(photosRepository).getPhotosList()
    }

    @Test
    fun buildUseCaseObservable_completes() {
        // Arrange
        stubPhotoRepositoryGetPhotosList(Single.just(PhotoDomainModelFactory.generateDummyPhtoDonainList(3)))

        // Act
        val testObserver = getPhotoListUseCase.buildUseCaseObservable().test()

        // Assert
        testObserver.assertComplete()
    }

    @Test
    fun buildUseCaseObservable_returnsData() {
        // Arrange
        val photoList = PhotoDomainModelFactory.generateDummyPhtoDonainList(4)
        stubPhotoRepositoryGetPhotosList(Single.just(photoList))

        // Act
        val testObserver = getPhotoListUseCase.buildUseCaseObservable().test()

        // Assert
        testObserver.assertValue(photoList)
    }

    private fun stubPhotoRepositoryGetPhotosList(single: Single<List<PhotoDomainModel>>) {
        `when`(photosRepository.getPhotosList())
            .thenReturn(single)
    }
}
