package com.reza.pixapp.domain.factory

import com.reza.pixapp.domain.model.PhotoDomainModel

object PhotoDomainModelFactory {

    fun generateDummyPhtoDonainList(size: Int): List<PhotoDomainModel> {
        val photoList = mutableListOf<PhotoDomainModel>()
        repeat(size) {
            photoList.add(generatePhotoDomainModel())
        }
        return photoList
    }

    fun generatePhotoDomainModel(): PhotoDomainModel {
        return PhotoDomainModel(
            id = DataFactory.getRandomInt(),
            albumId = DataFactory.getRandomInt(),
            title = DataFactory.getRandomString(),
            url = DataFactory.getRandomString(),
            thumbnailUrl = DataFactory.getRandomString()
        )
    }
}
