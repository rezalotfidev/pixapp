package com.reza.pixapp.domain.model

data class PhotoDomainModel(
    val albumId: Int,
    val id: Int,
    val thumbnailUrl: String,
    val title: String,
    val url: String
)