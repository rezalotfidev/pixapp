package com.reza.pixapp.domain.usecase

import com.reza.pixapp.domain.base.SingleUseCase
import com.reza.pixapp.domain.executor.PostExecutionThread
import com.reza.pixapp.domain.executor.ThreadExecutor
import com.reza.pixapp.domain.model.PhotoDomainModel
import com.reza.pixapp.domain.repositories.PhotosRepository
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GetPhotoDetailUseCase @Inject constructor(
    private val photosRepository: PhotosRepository,
    threadExecutor: ThreadExecutor,
    postExecutionThread: PostExecutionThread
) : SingleUseCase<Int, PhotoDomainModel>(
    threadExecutor, postExecutionThread
) {
    override fun buildUseCaseObservable(requestValues: Int?): Single<PhotoDomainModel> {
        return photosRepository.getPhotoDetails(requestValues ?: -1)
    }

}