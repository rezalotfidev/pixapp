package com.reza.pixapp.domain.di

import com.reza.pixapp.domain.usecase.GetPhotoDetailUseCase
import com.reza.pixapp.domain.usecase.GetPhotoListUseCase
import org.koin.dsl.module

val domainModule = module {

    // GetPhotosListUseCase
    single {
        GetPhotoListUseCase(get(),get(),get())
    }

    // GetPhotoDetailUseCase
    single {
        GetPhotoDetailUseCase(get(),get(),get())
    }

}