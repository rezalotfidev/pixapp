package com.reza.pixapp.domain.repositories

import com.reza.pixapp.domain.model.PhotoDomainModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

interface PhotosRepository {
    fun getPhotosList(): Single<List<PhotoDomainModel>>
    fun getPhotoDetails(id: Int): Single<PhotoDomainModel>
    fun savePhotos(listPhotos: List<PhotoDomainModel>): Completable
}
