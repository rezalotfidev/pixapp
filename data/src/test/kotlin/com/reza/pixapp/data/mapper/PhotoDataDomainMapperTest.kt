package com.reza.pixapp.data.mapper

import com.reza.pixapp.data.factory.PhotoDataModelFactory
import com.reza.pixapp.data.models.PhotoDataModel
import com.reza.pixapp.domain.model.PhotoDomainModel
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class PhotoDataDomainMapperTest {

    private lateinit var photoDataDomainMapper: PhotoDataDomainMapper

    @Before
    fun setUp() {
        photoDataDomainMapper = PhotoDataDomainMapper()
    }

    @Test
    fun mapFromEntity() {
        // Arrange
        val photoEntity = PhotoDataModelFactory.generatePhotoDataModel()

        // Act
        val photo = photoDataDomainMapper.mapFromData(photoEntity)

        // Assert
        assertPhotoDataEquality(photoEntity, photo)
    }

    @Test
    fun mapToEntity() {
        // Arrange
        val photo = PhotoDataModelFactory.generatePhotoDomain()

        // Act
        val photoEntity = photoDataDomainMapper.mapToData(photo)

        // Assert
        assertPhotoDataEquality(photoEntity, photo)
    }

    /**
     * Helper Methods
     */
    private fun assertPhotoDataEquality(photoDataModel: PhotoDataModel, photoDomainModel: PhotoDomainModel) {
        assertEquals(photoDataModel.id, photoDomainModel.id)
        assertEquals(photoDataModel.albumId, photoDomainModel.albumId)
        assertEquals(photoDataModel.title, photoDomainModel.title)
        assertEquals(photoDataModel.url, photoDomainModel.url)
        assertEquals(photoDataModel.thumbnailUrl, photoDomainModel.thumbnailUrl)
    }
}
