package com.reza.pixapp.data.store

import com.reza.pixapp.data.factory.PhotoDataModelFactory
import com.reza.pixapp.data.models.PhotoDataModel
import com.reza.pixapp.data.repository.PhotosRemote
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PhotosRemoteDataStoreTest {

    @Mock
    private lateinit var photoRemote: PhotosRemote

    private lateinit var photoRemoteDataStore: PhotoRemoteDataStore

    @Before
    fun setUp() {
        photoRemoteDataStore = PhotoRemoteDataStore(photoRemote)
    }

    @Test
    fun photoRemoteDataStore_getPhotos_returnsData() {
        // Arrange
        val photoList = PhotoDataModelFactory.generateDummyPhotoDataModels(5)
        stubPhotoRemoteGetPopularPhotos(Single.just(photoList))

        // Act
        val testObserver = photoRemoteDataStore.getAllPhotos().test()

        // Assert
        testObserver.assertValue(photoList)
    }

    /**
     * Stub Helper methods
     */
    private fun stubPhotoRemoteGetPopularPhotos(single: Single<List<PhotoDataModel>>) {
        `when`(photoRemote.getPhotosList())
            .thenReturn(single)
    }
}
