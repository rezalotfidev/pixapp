package com.reza.pixapp.data.store

import com.reza.pixapp.data.factory.PhotoDataModelFactory
import com.reza.pixapp.data.repository.PhotosCache
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PhotoCacheDataStoreTest {

    @Mock
    private lateinit var photoCache: PhotosCache

    private lateinit var photoCacheDataStore: PhotoCacheDataStore

    @Before
    fun setUp() {
        photoCacheDataStore = PhotoCacheDataStore(photoCache)
    }

    @Test
    fun photoCacheDataStore_savePhotos_completes() {

        // Act
        val testObserver =
            photoCacheDataStore.savePhotos(PhotoDataModelFactory.generateDummyPhotoDataModels(3)).test()

        // Assert
        testObserver.assertComplete()
    }



}
