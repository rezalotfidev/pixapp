package com.reza.pixapp.data.store

import com.reza.pixapp.data.repository.PhotosCache
import com.reza.pixapp.data.repository.PhotosRemote
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PhotoDataStoreFactoryTest {

    @Mock
    lateinit var photoCache: PhotosCache

    @Mock
    lateinit var photoRemote: PhotosRemote

    private lateinit var photoCacheDataStore: PhotoCacheDataStore

    private lateinit var photoRemoteDataStore: PhotoRemoteDataStore

    private lateinit var photoDataStoreFactory: PhotoDataStoreFactory

    @Before
    fun setUp() {
        photoCacheDataStore = PhotoCacheDataStore(photoCache)
        photoRemoteDataStore = PhotoRemoteDataStore(photoRemote)
        photoDataStoreFactory =
            PhotoDataStoreFactory(photoCache, photoCacheDataStore, photoRemoteDataStore)
    }

    @Test
    fun photoDataStoreFactory_getDataStore_noCache_returnRemoteStore() {
        // Arrange
        stubPhotoCacheIsCached(Single.just(false))

        // Act
        val photoDataStore = photoDataStoreFactory.getDataStore(false)

        // Assert
        assert(photoDataStore is PhotoRemoteDataStore)
    }

    @Test
    fun photoDataStoreFactory_getDataStore_expiredCache_returnRemoteStore() {
        // Arrange
        stubPhotoCacheIsCached(Single.just(true))
        stubPhotoCacheIsExpired(true)

        // Act
        val photoDataStore = photoDataStoreFactory.getDataStore(true)

        // Assert
        assert(photoDataStore is PhotoRemoteDataStore)
    }

    @Test
    fun photoDataStoreFactory_getDataStore_returnRemoteDataStore() {
        // Act
        val photoDataStore = photoDataStoreFactory.getRemoteDataStore()

        // Assert
        assert(photoDataStore is PhotoRemoteDataStore)
    }

    @Test
    fun photoDataStoreFactory_getDataStore_returnCacheDataStore() {
        // Act
        val photoDataStore = photoDataStoreFactory.getCacheDataStore()

        // Assert
        assert(photoDataStore is PhotoCacheDataStore)
    }

    /**
     * Stub helper methods
     */
    private fun stubPhotoCacheIsCached(single: Single<Boolean>) {
        `when`(photoCache.isCached())
            .thenReturn(single)
    }

    private fun stubPhotoCacheIsExpired(isExpired: Boolean) {
        `when`(photoCache.isExpired()).thenReturn(isExpired)
    }
}
