package com.reza.pixapp.data.factory

import com.reza.pixapp.data.models.PhotoDataModel
import com.reza.pixapp.domain.model.PhotoDomainModel

object PhotoDataModelFactory {

    fun generateDummyPhotoDataModels(size: Int): List<PhotoDataModel> {
        val mutablePhotoDataModelsList = mutableListOf<PhotoDataModel>()
        repeat(size) {
            mutablePhotoDataModelsList.add(generatePhotoDataModel())
        }

        return mutablePhotoDataModelsList
    }

    fun generateDummyPhotoDomainList(size: Int): List<PhotoDomainModel> {
        val photoList = mutableListOf<PhotoDomainModel>()
        repeat(size) {
            photoList.add(generatePhotoDomain())
        }
        return photoList
    }

    fun generatePhotoDataModel(): PhotoDataModel {
        return PhotoDataModel(
            id = DataFactory.getRandomInt(),
            albumId = DataFactory.getRandomInt(),
            title = DataFactory.getRandomString(),
            url = DataFactory.getRandomString(),
            thumbnailUrl = DataFactory.getRandomString()
        )
    }

    fun generatePhotoDomain():PhotoDomainModel{
        return PhotoDomainModel(
            id = DataFactory.getRandomInt(),
            albumId = DataFactory.getRandomInt(),
            title = DataFactory.getRandomString(),
            url = DataFactory.getRandomString(),
            thumbnailUrl = DataFactory.getRandomString()
        )
    }
}
