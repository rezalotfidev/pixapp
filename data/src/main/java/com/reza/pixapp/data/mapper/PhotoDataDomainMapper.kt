package com.reza.pixapp.data.mapper

import com.reza.pixapp.data.models.PhotoDataModel
import com.reza.pixapp.domain.model.PhotoDomainModel
import javax.inject.Inject

class PhotoDataDomainMapper{

    fun mapFromData(model: PhotoDataModel): PhotoDomainModel {
        return PhotoDomainModel(
            id = model.id,
            albumId = model.albumId,
            thumbnailUrl = model.thumbnailUrl,
            title = model.title,
            url = model.url
        )
    }

    fun mapToData(model: PhotoDomainModel): PhotoDataModel {
        return PhotoDataModel(
            id = model.id,
            albumId = model.albumId,
            thumbnailUrl = model.thumbnailUrl,
            title = model.title,
            url = model.url
        )
    }
}
