package com.reza.pixapp.data.di

import com.reza.pixapp.data.PhotosRepositoryImpl
import com.reza.pixapp.data.executor.JobExecutor
import com.reza.pixapp.data.mapper.PhotoDataDomainMapper
import com.reza.pixapp.data.store.PhotoCacheDataStore
import com.reza.pixapp.data.store.PhotoDataStoreFactory
import com.reza.pixapp.data.store.PhotoRemoteDataStore
import com.reza.pixapp.domain.executor.ThreadExecutor
import com.reza.pixapp.domain.repositories.PhotosRepository
import com.reza.pixapp.domain.usecase.GetPhotoDetailUseCase
import com.reza.pixapp.domain.usecase.GetPhotoListUseCase
import org.koin.dsl.module

val dataModule = module {

    // PhotoDataDomainMapper
    single {
        PhotoDataDomainMapper()
    }

    // PhotosRepository
    single<PhotosRepository> {
        PhotosRepositoryImpl(get(),get())
    }

    // JobExecutor
    single<ThreadExecutor> {
        JobExecutor()
    }

    // PhotoDataStoreFactory
    single {
        PhotoDataStoreFactory(get(),get(),get())
    }

    // PhotoCacheDataStore
    single {
        PhotoCacheDataStore(get())
    }

    // PhotoRemoteDataStore
    single {
        PhotoRemoteDataStore(get())
    }
}