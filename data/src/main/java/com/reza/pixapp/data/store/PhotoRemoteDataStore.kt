package com.reza.pixapp.data.store

import com.reza.pixapp.data.repository.PhotoDataStore
import com.reza.pixapp.data.repository.PhotosRemote
import com.reza.pixapp.data.models.PhotoDataModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class PhotoRemoteDataStore(
    private val photosRemote: PhotosRemote
) : PhotoDataStore {

    override fun getPhotoById(id: Int): Single<PhotoDataModel> {
        return photosRemote.getPhotoDetails(id)
    }

    override fun getAllPhotos(): Single<List<PhotoDataModel>> {
        return photosRemote.getPhotosList()
    }

    override fun isCached(): Single<Boolean> {
        return Single.just(false)
    }

    override fun savePhotos(listPhotos: List<PhotoDataModel>): Completable {
        throw UnsupportedOperationException("this photo action not applicable for remote.")
    }
}