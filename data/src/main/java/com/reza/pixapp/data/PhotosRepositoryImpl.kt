package com.reza.pixapp.data

import com.reza.pixapp.data.mapper.PhotoDataDomainMapper
import com.reza.pixapp.data.store.PhotoDataStoreFactory
import com.reza.pixapp.domain.repositories.PhotosRepository
import com.reza.pixapp.domain.model.PhotoDomainModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class PhotosRepositoryImpl @Inject constructor(
    private val photoDataDomainMapper: PhotoDataDomainMapper,
    private val photoDataStoreFactory: PhotoDataStoreFactory
) : PhotosRepository {

    override fun getPhotosList(): Single<List<PhotoDomainModel>> {
        return photoDataStoreFactory.getCacheDataStore().isCached()
            .subscribeOn(Schedulers.io())
            .flatMap {
                photoDataStoreFactory.getDataStore(it).getAllPhotos()
            }
            .flatMap { list ->
                Single.just(list.map { photoDataDomainMapper.mapFromData(it) })
            }
            .flatMap {
                savePhotos(it).toSingle { it }
            }
    }

    override fun getPhotoDetails(id: Int): Single<PhotoDomainModel> {
        return photoDataStoreFactory.getCacheDataStore().isCached()
            .subscribeOn(Schedulers.io())
            .flatMap {
                photoDataStoreFactory.getDataStore(it).getPhotoById(id)
            }
            .flatMap {
                Single.just(photoDataDomainMapper.mapFromData(it))
            }
    }

    override fun savePhotos(listPhotos: List<PhotoDomainModel>): Completable {
        return photoDataStoreFactory.getCacheDataStore().savePhotos(
            listPhotos.map { photoDataDomainMapper.mapToData(it) }
        )
    }

}
