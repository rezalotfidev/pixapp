package com.reza.pixapp.data.repository

import com.reza.pixapp.data.models.PhotoDataModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

interface PhotoDataStore {
    fun getPhotoById(id: Int): Single<PhotoDataModel>
    fun getAllPhotos() : Single<List<PhotoDataModel>>
    fun savePhotos(listPhotos: List<PhotoDataModel>): Completable
    fun isCached(): Single<Boolean>
}
