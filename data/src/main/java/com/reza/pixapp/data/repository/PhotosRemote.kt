package com.reza.pixapp.data.repository

import com.reza.pixapp.data.models.PhotoDataModel
import io.reactivex.rxjava3.core.Single

interface PhotosRemote {
    fun getPhotosList(): Single<List<PhotoDataModel>>

    fun getPhotoDetails(id: Int): Single<PhotoDataModel>
}
