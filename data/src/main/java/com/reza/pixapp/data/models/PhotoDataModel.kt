package com.reza.pixapp.data.models

data class PhotoDataModel(
    val albumId: Int,
    val id: Int,
    val thumbnailUrl: String,
    val title: String,
    val url: String
)