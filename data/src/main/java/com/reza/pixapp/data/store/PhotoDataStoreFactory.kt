package com.reza.pixapp.data.store

import com.reza.pixapp.data.repository.PhotoDataStore
import com.reza.pixapp.data.repository.PhotosCache
import javax.inject.Inject

open class PhotoDataStoreFactory(
    private val photosCache: PhotosCache,
    private val cacheDataStore: PhotoCacheDataStore,
    private val remoteDataStore: PhotoRemoteDataStore
) {

    open fun getDataStore(isCached: Boolean): PhotoDataStore {
        if (isCached && !photosCache.isExpired()) {
            return getCacheDataStore()
        }
        return getRemoteDataStore()
    }

    fun getCacheDataStore(): PhotoDataStore {
        return cacheDataStore
    }

    fun getRemoteDataStore(): PhotoDataStore {
        return remoteDataStore
    }
}
