package com.reza.pixapp.data.store

import com.reza.pixapp.data.repository.PhotoDataStore
import com.reza.pixapp.data.repository.PhotosCache
import com.reza.pixapp.data.models.PhotoDataModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class PhotoCacheDataStore @Inject constructor(
    private val photosCache: PhotosCache
) : PhotoDataStore {

    override fun getPhotoById(id: Int): Single<PhotoDataModel> {
        return photosCache.getPhotoById(id)
    }

    override fun getAllPhotos(): Single<List<PhotoDataModel>> {
        return photosCache.getAllPhotos()
    }

    override fun savePhotos(listPhotos: List<PhotoDataModel>): Completable {
        return photosCache.savePhotos(listPhotos).doOnComplete {
            photosCache.setLastCacheTime(System.currentTimeMillis())
        }
    }

    override fun isCached(): Single<Boolean> {
        return photosCache.isCached()
    }
}
