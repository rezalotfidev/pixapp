package com.reza.pixapp.cache.mapper

interface CacheDataMapper<T, V> {

    fun mapFromCached(type: T): V

    fun mapToCached(type: V): T
}
