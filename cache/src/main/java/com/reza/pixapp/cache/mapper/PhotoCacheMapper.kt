package com.reza.pixapp.cache.mapper

import com.reza.pixapp.cache.models.CachedPhoto
import com.reza.pixapp.data.models.PhotoDataModel

class PhotoCacheMapper {
    fun mapFromCached(entity: CachedPhoto): PhotoDataModel {
        return PhotoDataModel(
            id = entity.id.toInt(),
            albumId = entity.albumId,
            thumbnailUrl = entity.thumbnailUrl,
            title = entity.title,
            url = entity.url
        )
    }

    fun mapToCached(model: PhotoDataModel): CachedPhoto {
        return CachedPhoto(
            id = model.id.toLong(),
            albumId = model.albumId,
            thumbnailUrl = model.thumbnailUrl,
            title = model.title,
            url = model.url
        )
    }
}
