package com.reza.pixapp.cache.db

object CacheConstants {
    const val DB_NAME = "pixapp.db"
    const val PHOTOS_TABLE_NAME = "photos"
}
