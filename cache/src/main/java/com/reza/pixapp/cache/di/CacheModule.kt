package com.reza.pixapp.cache.di

import com.reza.pixapp.cache.PhotosCacheImp
import com.reza.pixapp.cache.PreferencesHelper
import com.reza.pixapp.cache.db.PhotoDatabase
import com.reza.pixapp.cache.mapper.PhotoCacheMapper
import com.reza.pixapp.data.repository.PhotosCache
import org.koin.dsl.module

val cacheModule = module {

    // Database
    single {
        PhotoDatabase.getInstance(get())
    }

    // PhotoCacheMapper
    single {
        PhotoCacheMapper()
    }

    // PhotoCache
    single<PhotosCache> {
        PhotosCacheImp(get(),get(),get())
    }

    // PreferencesHelper
    single {
        PreferencesHelper(get())
    }
}