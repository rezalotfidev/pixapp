package com.reza.pixapp.cache

import com.reza.pixapp.cache.db.PhotoDatabase
import com.reza.pixapp.cache.mapper.PhotoCacheMapper
import com.reza.pixapp.data.repository.PhotosCache
import com.reza.pixapp.data.models.PhotoDataModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class PhotosCacheImp @Inject constructor(
    private val photoDatabase: PhotoDatabase,
    private val photoCacheMapper: PhotoCacheMapper,
    private val preferencesHelper: PreferencesHelper
) : PhotosCache {

    override fun getAllPhotos(): Single<List<PhotoDataModel>> {
        return Single.defer {
            Single.just(photoDatabase.cachedPhotoDao().getAllPhotos()).map { list ->
                list.map { photoCacheMapper.mapFromCached(it) }
            }
        }
    }

    override fun getPhotoById(id: Int): Single<PhotoDataModel> {
        return Single.defer {
            Single.just(photoDatabase.cachedPhotoDao().getPhotoById(id.toLong())).map {
                photoCacheMapper.mapFromCached(it)
            }
        }
    }

    override fun savePhotos(listPhotos: List<PhotoDataModel>): Completable {
        return Completable.defer {
            listPhotos.map { photoCacheMapper.mapToCached(it) }.forEach {
                photoDatabase.cachedPhotoDao().addPhoto(it)
            }
            Completable.complete()
        }
    }

    override fun isCached(): Single<Boolean> {
        return Single.defer {
            Single.just(photoDatabase.cachedPhotoDao().getPhotosCount() > 0)
        }
    }

    override fun setLastCacheTime(lastCache: Long) {
        preferencesHelper.lastCacheTime = lastCache
    }

    override fun isExpired(): Boolean {
        val currentTime = System.currentTimeMillis()
        val lastUpdateTime = this.getLastCacheUpdateTimeMillis()
        return currentTime - lastUpdateTime > EXPIRATION_TIME
    }

    /**
     * Get in millis, the last time the cache was accessed.
     */
    private fun getLastCacheUpdateTimeMillis(): Long {
        return preferencesHelper.lastCacheTime
    }

    companion object {
        /**
         * Expiration time set to 5 minutes
         */
        const val EXPIRATION_TIME = (60 * 5 * 1000).toLong()
    }
}
