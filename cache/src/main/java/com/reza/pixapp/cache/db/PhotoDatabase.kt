package com.reza.pixapp.cache.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.reza.pixapp.cache.dao.CachedPhotoDao
import com.reza.pixapp.cache.models.CachedPhoto
import javax.inject.Inject

@Database(entities = [CachedPhoto::class], version = 1)
abstract class PhotoDatabase @Inject constructor() : RoomDatabase() {

    abstract fun cachedPhotoDao(): CachedPhotoDao

    companion object {
        @Volatile
        private var INSTANCE: PhotoDatabase? = null

        fun getInstance(context: Context): PhotoDatabase = INSTANCE ?: synchronized(this) {
            INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext, PhotoDatabase::class.java, CacheConstants.DB_NAME
        ).build()
    }
}
