package com.reza.pixapp.cache.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.reza.pixapp.cache.db.CacheConstants

@Entity(tableName = CacheConstants.PHOTOS_TABLE_NAME)
data class CachedPhoto(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Long,

    @ColumnInfo(name = "album_id")
    val albumId: Int,

    @ColumnInfo(name = "thumbnail_url")
    val thumbnailUrl: String,

    @ColumnInfo(name = "title")
    val title: String,

    @ColumnInfo(name = "url")
    val url: String
)
