package com.reza.pixapp.cache.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.reza.pixapp.cache.models.CachedPhoto

@Dao
interface CachedPhotoDao {

    @Query("SELECT * FROM photos")
    fun getAllPhotos(): List<CachedPhoto>

    @Query("SELECT COUNT(*) FROM photos")
    fun getPhotosCount(): Int

    @Query("SELECT * FROM photos WHERE id = :id LIMIT 1")
    fun getPhotoById(id: Long): CachedPhoto

    @Query("DELETE FROM photos")
    fun clearPhotos()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addPhoto(cachedPhoto: CachedPhoto)
}
