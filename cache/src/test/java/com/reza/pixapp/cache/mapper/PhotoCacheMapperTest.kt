package com.reza.pixapp.cache.mapper

import com.reza.pixapp.cache.models.CachedPhoto
import com.reza.pixapp.cache.test.factory.CachedPhotosFactory
import com.reza.pixapp.data.models.PhotoDataModel
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PhotoCacheMapperTest {

    lateinit var photoEntityMapper: PhotoCacheMapper

    @Before
    fun setUp() {
        photoEntityMapper = PhotoCacheMapper()
    }

    @Test
    fun mapFromCached() {
        // Arrange
        val cachePhoto = CachedPhotosFactory.generateCachedPhoto()

        // Act
        val photosDataModels = photoEntityMapper.mapFromCached(cachePhoto)

        // Assert
        assertPhotoDataModelEquality(cachePhoto, photosDataModels)
    }

    @Test
    fun mapToCache() {
        // Arrange
        val photosDataModel = CachedPhotosFactory.generatePhotoDataModel()

        // Act
        val cachePhoto = photoEntityMapper.mapToCached(photosDataModel)

        // Assert
        assertPhotoDataModelEquality(cachePhoto, photosDataModel)
    }

    private fun assertPhotoDataModelEquality(cachedPhoto: CachedPhoto, photoDataModel: PhotoDataModel) {
        Assert.assertEquals(photoDataModel.id, cachedPhoto.id)
        Assert.assertEquals(photoDataModel.albumId, cachedPhoto.albumId)
        Assert.assertEquals(photoDataModel.url, cachedPhoto.url)
        Assert.assertEquals(photoDataModel.thumbnailUrl, cachedPhoto.thumbnailUrl)
    }

}
