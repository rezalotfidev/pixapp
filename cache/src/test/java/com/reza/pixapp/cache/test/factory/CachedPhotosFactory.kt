package com.reza.pixapp.cache.test.factory

import com.reza.pixapp.cache.models.CachedPhoto
import com.reza.pixapp.data.models.PhotoDataModel

object CachedPhotosFactory {

    fun generateListOfCachedPhotos(size: Int): MutableList<CachedPhoto> {
        val listOfCachedPhoto = mutableListOf<CachedPhoto>()
        repeat(size) {
            listOfCachedPhoto.add(generateCachedPhoto())
        }
        return listOfCachedPhoto
    }

    fun generateListOfPhotoDataModels(size: Int): MutableList<PhotoDataModel> {
        val listOfPhotoDataModels = mutableListOf<PhotoDataModel>()
        repeat(size) {
            listOfPhotoDataModels.add(generatePhotoDataModel())
        }
        return listOfPhotoDataModels
    }

    fun generateCachedPhoto(): CachedPhoto {
        return CachedPhoto(
            id = DataFactory.getRandomLong(),
            albumId = DataFactory.getRandomInt(),
            title = DataFactory.getRandomString(),
            url = DataFactory.getRandomString(),
            thumbnailUrl = DataFactory.getRandomString()
        )
    }

    fun generatePhotoDataModel(): PhotoDataModel {
        return PhotoDataModel(
            id = DataFactory.getRandomInt(),
            albumId = DataFactory.getRandomInt(),
            title = DataFactory.getRandomString(),
            url = DataFactory.getRandomString(),
            thumbnailUrl = DataFactory.getRandomString()
        )
    }
}
