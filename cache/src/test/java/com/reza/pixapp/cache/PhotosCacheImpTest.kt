package com.reza.pixapp.cache

import androidx.room.Room
import com.reza.pixapp.cache.db.PhotoDatabase
import com.reza.pixapp.cache.mapper.PhotoCacheMapper
import com.reza.pixapp.cache.test.factory.CachedPhotosFactory
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config

@Config(manifest = Config.NONE)
@RunWith(RobolectricTestRunner::class)
class PhotosCacheImpTest {

    private lateinit var photoDatabase: PhotoDatabase

    private lateinit var photoEntityMapper: PhotoCacheMapper

    private lateinit var preferencesHelper: PreferencesHelper

    private lateinit var photosCacheImp: PhotosCacheImp

    @Suppress("DEPRECATION")
    @Before
    fun setUp() {
        preferencesHelper = PreferencesHelper(RuntimeEnvironment.application)
        photoEntityMapper = PhotoCacheMapper()
        photoDatabase = Room.inMemoryDatabaseBuilder(
            RuntimeEnvironment.application.baseContext,
            PhotoDatabase::class.java
        )
            .allowMainThreadQueries()
            .build()
        photosCacheImp = PhotosCacheImp(photoDatabase, photoEntityMapper, preferencesHelper)
    }

    @Test
    fun savePhotos_completes() {
        // Arrange
        val photoDataModels = CachedPhotosFactory.generateListOfPhotoDataModels(7)

        // Act
        val testObserver = photosCacheImp.savePhotos(photoDataModels).test()

        // Assert
        testObserver.assertComplete()
    }

    @Test
    fun savePhotos_dataSaved() {
        // Arrange
        val photoDataModels = CachedPhotosFactory.generateListOfPhotoDataModels(7)

        // Act
        photosCacheImp.savePhotos(photoDataModels).test()

        // Assert
        val cachedPhotos = photoDatabase.cachedPhotoDao().getAllPhotos()
        assert(cachedPhotos.size == photoDataModels.size)
    }

    @Test
    fun getPhotos_returnsData() {
        // Arrange
        val photoDataModels = CachedPhotosFactory.generateListOfPhotoDataModels(7)
        photosCacheImp.savePhotos(photoDataModels).test()

        // Act
        val testObserver = photosCacheImp.getAllPhotos().test()

        // Assert
        assert(testObserver.values()[0].size == photoDataModels.size)
    }

    @Test
    fun getPhotos_completes() {
        // Arrange
        val photoDataModels = CachedPhotosFactory.generateListOfPhotoDataModels(7)
        photosCacheImp.savePhotos(photoDataModels).test()

        // Act
        val testObserver = photosCacheImp.getAllPhotos().test()

        // Assert
        testObserver.assertComplete()
    }

    @Test
    fun isCached_responseTrue() {
        // Arrange
        val photoDataModels = CachedPhotosFactory.generateListOfPhotoDataModels(7)
        photosCacheImp.savePhotos(photoDataModels).test()

        // Act
        val testObserver = photosCacheImp.isCached().test()

        // Assert
        testObserver.assertValue(true)
    }

    @Test
    fun isCached_responseFalse() {
        // Arrange
        // No arrange needed.

        // Act
        val testObserver = photosCacheImp.isCached().test()

        // Assert
        testObserver.assertValue(false)
    }

    @Test
    fun isExpired_responseTrue() {
        // Arrange
        val currentTimeInMillis = System.currentTimeMillis()
        val expirationTime = PhotosCacheImp.EXPIRATION_TIME

        val photoDataModels = CachedPhotosFactory.generateListOfPhotoDataModels(7)
        photosCacheImp.savePhotos(photoDataModels).test()
        photosCacheImp.setLastCacheTime(currentTimeInMillis.minus(expirationTime))

        // Act
        val isExpired = photosCacheImp.isExpired()

        // Assert
        assert(isExpired)
    }

    @Test
    fun isExpired_responseFalse() {
        // Arrange
        val photoDataModels = CachedPhotosFactory.generateListOfPhotoDataModels(7)
        photosCacheImp.savePhotos(photoDataModels).test()
        photosCacheImp.setLastCacheTime(System.currentTimeMillis())

        // Act
        val isExpired = photosCacheImp.isExpired()

        // Assert
        assert(isExpired.not())
    }

    @After
    fun closeDb() {
        photoDatabase.close()
    }
}
