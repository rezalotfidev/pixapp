package com.reza.pixapp.cache.dao

import androidx.room.Room
import com.reza.pixapp.cache.db.PhotoDatabase
import com.reza.pixapp.cache.test.factory.CachedPhotosFactory
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config

@Config(manifest = Config.NONE)
@RunWith(RobolectricTestRunner::class)
class CachedPhotoDaoTest {

    lateinit var photoDatabase: PhotoDatabase

    @Before
    fun setUp() {
        photoDatabase = Room.inMemoryDatabaseBuilder(
            RuntimeEnvironment.getApplication().baseContext,
            PhotoDatabase::class.java)
            .allowMainThreadQueries()
            .build()
    }

    @Test
    fun addPhotoToDatabase() {
        // Arrange
        val cachedPhoto = CachedPhotosFactory.generateCachedPhoto()

        // Act
        photoDatabase.cachedPhotoDao().addPhoto(cachedPhoto)

        // Assert
        val cachedPhotos = photoDatabase.cachedPhotoDao().getAllPhotos()
        assert(cachedPhotos.isNotEmpty())
    }


    @Test
    fun clearPhotos_completes() {
        // Arrange
        val cachedPhotos = CachedPhotosFactory.generateListOfCachedPhotos(8)
        repeat(cachedPhotos.size) {
            photoDatabase.cachedPhotoDao().addPhoto(cachedPhotos[it])
        }

        // Act
        photoDatabase.cachedPhotoDao().clearPhotos()

        // Assert
        val photos = photoDatabase.cachedPhotoDao().getAllPhotos()
        assert(photos.isEmpty())
    }

    @Test
    fun getPhotos_returnData() {
        // Arrange
        val cachedPhotos = CachedPhotosFactory.generateListOfCachedPhotos(8)
        repeat(cachedPhotos.size) {
            photoDatabase.cachedPhotoDao().addPhoto(cachedPhotos[it])
        }

        // Act
        val photos = photoDatabase.cachedPhotoDao().getAllPhotos()

        // Assert
        assert(photos.size == cachedPhotos.size)
    }

    @Test
    fun getPhotos_returnEmpty() {
        // Arrange
        // No Arrangement needed

        // Act
        val photos = photoDatabase.cachedPhotoDao().getAllPhotos()

        // Assert
        assert(photos.isEmpty())
    }

    @After
    fun closeDb() {
        photoDatabase.close()
    }
}
