# Pix App project

This is an Android application for searching and exploring images from [Jsonplaceholder-API](https://jsonplaceholder.typicode.com/photos) and created for Payback Group interview process.

It simply loads **Images** list from [Jsonplaceholder-API](https://jsonplaceholder.typicode.com/photos)  and stores it in persistence storage (i.e. Room Database). Photos list will be always loaded from local database. Remote data (from API) and Local data is always synchronized.
It has feature of bookmark photo which will be stored locally.
- Modular Clean Architecture approach followed
- It is heavily implemented by following standard clean architecture principle.
- Offline capability.
- It has cache expiration feature [5 minutes] and it can be increased.
- Unit testing written for all layers.
- Multi Module Code Coverage reports using Jacoco.
- static code analyses added by using detekt, ktlint.
- [S.O.L.I.D](https://en.wikipedia.org/wiki/SOLID) principle followed for more understandable, flexible and maintainable.

## - Screenshots

![](/screenshot/screen1.png)
![](/screenshot/screen2.png)

## - Tech Stack 🛠
- [Kotlin](https://kotlinlang.org/) - First class and official programming language for Android development.
- [Rx-Java](https://github.com/ReactiveX/RxJava) - For composing asynchronous and event-based programs by using observable sequences.
- [Android Architecture Components](https://developer.android.com/topic/libraries/architecture) - Collection of libraries that help you design robust, testable, and maintainable apps.
  - [LiveData](https://developer.android.com/topic/libraries/architecture/livedata) - Data objects that notify views when the underlying database changes.
  - [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel) - Stores UI-related data that isn't destroyed on UI changes. 
  - [Room](https://developer.android.com/topic/libraries/architecture/room) - SQLite object mapping library.
- [Koin DI](https://insert-koin.io/) - Dependency Injection Framework
- [Retrofit](https://square.github.io/retrofit/) - A type-safe HTTP client for Android and Java.
- [OkHttp](http://square.github.io/okhttp/) - HTTP client that's efficient by default: HTTP/2 support allows all requests to the same host to share a socket
- [Glide](https://github.com/bumptech/glide) - image loading framework for Android
- [Gson](https://github.com/google/gson) - used to convert Java Objects into their JSON representation and vice versa.
- [Mockito](http://site.mockito.org/) - Most popular mocking framework for Java/kotlin.
- [Robolectric](http://robolectric.org/) - allows you to write unit tests and run them on a desktop JVM while still using Android API.
- [Stetho](https://github.com/facebook/stetho) - Stetho is a debug bridge for Android applications, enabling the powerful Chrome Developer Tools and much more.
- [Detekt](https://detekt.github.io/detekt/) - Detekt is a static analyze tool for the Kotlin language. It's open source.
- [Ktlint](https://github.com/pinterest/ktlint) -Ktlint is a static code analysis tool maintain by Pinterest. Linter and formatter for Kotlin code.


### Clean Architecture and Why this approach?
1. Separation of code in different layers with assigned responsibilities making it easier for further modification.
2. High level of abstraction
3. Loose coupling between the code
4. Testing of code is painless

### Layers
- **Domain** - Would execute business logic which is independent of any layer and is just a pure kotlin/java package with no android specific dependency.
- **Data** - Would dispense the required data for the application to the domain layer by implementing interface exposed by the domain.
- **Presentation / framework** - Would include both domain and data layer and is android specific which executes the UI logic.

## - Modules of App
  ### App 
  It uses the all the components and class related to Android Framework. It gets the data from presentation layer and shows on UI.
  
  ### BuildSrc
  This module helps to list and manage all the dependencies of the app at one place. It has list of dependencies and versions of that dependencies.
  
  ### Data 
  The Data layer is our access point to external data layers and is used to fetch data from multiple sources (the cache and remote in our case).
  
  ### Cache
  The Cache layer handles all communication with the local database which is used to cache data.
  
  ### Domain
  The domain layer responsibility is to simply contain the UseCase instance used to retrieve data from the Data layer and pass it onto the Presentation layer. 
  
  ### Presentation
  This layer's responsibility is to handle the presentation of the User Interface, but at the same time knows nothing about the user interface itself. This layer has no dependence on the Android UI Framework. Each ViewModel class that is created implements the ViewModel class found within the Architecture components library. This ViewModel can then be used by the UI layer to communicate with UseCases and retrieve data.
  
  ### Remote
  The Remote layer handles all communications with remote sources, in our case it makes a simple API call using a Retrofit interface. 

## - Code Coverage Reports
This projects uses [jacoco plugin](https://www.jacoco.org/jacoco/) for generate code coverage reports. currently each modules generates seprate code reports.

  #### Java/Kotlin Modules
  Use `./gradlew jacocoTestReport`
  
  Reports location for each module-<br>
  `-/data/build/reports/jacoco/test/html/index.html`<br>
  `-/domain/build/reports/jacoco/test/html/index.html`<br>
  `-/remote/build/reports/jacoco/test/html/index.html`<br>

  #### Android Modules
  Use `./gradlew jacocoTestReportDebug`

  Reports location for each module-<br>
  `-/app/build/reports/jacoco/debug/index.html`<br>
  `-/cache/build/reports/jacoco/debug/index.html`<br>
  `-/presentattion/build/reports/jacoco/debug/index.html` <br>
 
 
## - Code Quality Checks
This projects uses detekt, ktlint static code analyser and auto formatter in CI/CD pipeline to maintain code quality. they also genrated reports.

  #### Run detekt locally
  Use `./gradlew detekt`
  
  Reports location for each module-<br>
  `-/cache/build/reports/detekt/report.html`<br>
  `-/domain/build/reports/detekt/report.html`<br>
  `-/remote/build/reports/detekt/report.html`<br>

  #### Run ktlint locally
  Use `./gradlew ktlint`

  Reports location for each module-<br>
  `-/app/build/reports/ktlint/ktlint-checkstyle-report.xml`<br>
  `-/cache/build/reports/ktlint/ktlint-checkstyle-report.xml`<br>
  `-/presentattion/build/reports/ktlint/ktlint-checkstyle-report.xml` <br>
 
  #### Run ktlint formatter locally
  Use `./gradlew ktlintFormat`

## - Question

  #### Two things that are solved well in this project
  1 - Code quality control
  2 - Architecture (Unit Tests, Clean Architecture, Reactive Programming, etc)
  
  #### Two things I would improve in a next step if you had more time
  1 - Working more on the UI tests
  2 - Improving gradle build time
