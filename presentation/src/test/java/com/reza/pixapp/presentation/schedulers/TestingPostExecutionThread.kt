package com.reza.pixapp.presentation.schedulers

import com.reza.pixapp.domain.executor.PostExecutionThread
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers

class TestingPostExecutionThread : PostExecutionThread {
    override val scheduler: Scheduler = Schedulers.trampoline()
}
