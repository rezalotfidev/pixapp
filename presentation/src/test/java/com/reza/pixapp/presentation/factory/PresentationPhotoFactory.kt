package com.reza.pixapp.presentation.factory

import com.reza.pixapp.domain.model.PhotoDomainModel
import com.reza.pixapp.presentation.model.PhotoUiModel

object PresentationPhotoFactory {

    fun generateListOfPhotos(size: Int): MutableList<PhotoDomainModel> {
        val listOfPhotos = mutableListOf<PhotoDomainModel>()
        repeat(size) {
            listOfPhotos.add(generatePhoto())
        }
        return listOfPhotos
    }

    fun generatePhotoView(): PhotoUiModel {
        return PhotoUiModel(
            id = DataFactory.getRandomInt(),
            albumId = DataFactory.getRandomInt(),
            title = DataFactory.getRandomString(),
            url = DataFactory.getRandomString(),
            thumbnailUrl = DataFactory.getRandomString()
        )
    }

    fun generatePhoto(): PhotoDomainModel {
        return PhotoDomainModel(
            id = DataFactory.getRandomInt(),
            albumId = DataFactory.getRandomInt(),
            title = DataFactory.getRandomString(),
            url = DataFactory.getRandomString(),
            thumbnailUrl = DataFactory.getRandomString()
        )
    }
}
