package com.reza.pixapp.presentation.viewmodel

import androidx.lifecycle.Observer
import androidx.paging.PagingData
import com.reza.pixapp.domain.executor.PostExecutionThread
import com.reza.pixapp.domain.executor.ThreadExecutor
import com.reza.pixapp.domain.model.PhotoDomainModel
import com.reza.pixapp.domain.repositories.PhotosRepository
import com.reza.pixapp.domain.usecase.GetPhotoListUseCase
import com.reza.pixapp.presentation.factory.PresentationPhotoFactory
import com.reza.pixapp.presentation.mapper.PhotoUiMapper
import com.reza.pixapp.presentation.model.PhotoUiModel
import com.reza.pixapp.presentation.viewmodel.photos.PhotosState
import com.reza.pixapp.presentation.viewmodel.photos.PhotosViewModel
import com.reza.pixapp.presentation.schedulers.TestingPostExecutionThread
import com.reza.pixapp.presentation.schedulers.TestingThreadExecutor
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class PhotoListViewModelTest {

    @Mock
    lateinit var photoRepository: PhotosRepository

    @Mock
    private lateinit var stateObserver: Observer<PhotosState>

    private var photoUiMapper = PhotoUiMapper()

    private lateinit var photosViewModel: PhotosViewModel

    private lateinit var getPhotoListUseCase: GetPhotoListUseCase

    private lateinit var threadExecutor: ThreadExecutor

    private lateinit var postExecutionThread: PostExecutionThread

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        setUpThreadForRxJava()
        setUpUseCases()
        setUpViewModel()
    }

    private fun setUpThreadForRxJava() {
        threadExecutor = TestingThreadExecutor()
        postExecutionThread = TestingPostExecutionThread()
    }

    private fun setUpUseCases() {
        getPhotoListUseCase =
            GetPhotoListUseCase(photoRepository, threadExecutor, postExecutionThread)
    }

    private fun setUpViewModel() {
        photosViewModel = PhotosViewModel(
            photoUiMapper,
            getPhotoListUseCase,
        )
        photosViewModel.stateObservable.observeForever(stateObserver)
    }

    @Test
    fun fetchPhotosList_returnsEmpty() {
        // Arrange
        stubFetchPhotos(Single.just(listOf()))

        // Act
        photosViewModel.fetchPhotosList()

        // Assert
        verify(stateObserver).onChanged(PhotosState.Loading)
        verify(stateObserver).onChanged(PhotosState.PhotoListSuccess(PagingData.from(listOf())))
    }

    @Test
    fun fetchPhotosList_returnsError() {
        // Arrange
        stubFetchPhotos(Single.error(TestingException(TestingException.GENERIC_EXCEPTION_MESSAGE)))

        // Act
        photosViewModel.fetchPhotosList()

        // Assert
        verify(stateObserver).onChanged(PhotosState.Loading)
        verify(stateObserver).onChanged(PhotosState.Error(TestingException.GENERIC_EXCEPTION_MESSAGE))
    }

    @Test
    fun fetchPhotosList_returnsData() {
        // Arrange
        val listOfPhotos = PresentationPhotoFactory.generateListOfPhotos(10)
        val listOfViews = mutableListOf<PhotoUiModel>()
        listOfPhotos.forEach {
            listOfViews.add(photoUiMapper.mapToView(it))
        }
        stubFetchPhotos(Single.just(listOfPhotos))

        // Act
        photosViewModel.fetchPhotosList()

        // Assert
        verify(stateObserver).onChanged(PhotosState.Loading)
        verify(stateObserver).onChanged(PhotosState.PhotoListSuccess(PagingData.from(listOfViews)))
    }

    /**
     * Stub Helpers Methods
     */
    private fun stubFetchPhotos(single: Single<List<PhotoDomainModel>>) {
        `when`(getPhotoListUseCase.buildUseCaseObservable())
            .thenReturn(single)
    }

    class TestingException(message: String = GENERIC_EXCEPTION_MESSAGE) : Exception(message) {
        companion object {
            const val GENERIC_EXCEPTION_MESSAGE = "Something error came while executing"
        }
    }
}
