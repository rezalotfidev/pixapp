package com.reza.pixapp.presentation.schedulers

import com.reza.pixapp.domain.executor.ThreadExecutor
import io.reactivex.rxjava3.schedulers.Schedulers

class TestingThreadExecutor : ThreadExecutor {
    override fun execute(command: Runnable?) {
        Schedulers.trampoline().scheduleDirect(command!!)
    }
}
