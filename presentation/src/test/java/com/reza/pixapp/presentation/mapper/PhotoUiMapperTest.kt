package com.reza.pixapp.presentation.mapper

import com.reza.pixapp.domain.model.PhotoDomainModel
import com.reza.pixapp.presentation.factory.PresentationPhotoFactory
import com.reza.pixapp.presentation.model.PhotoUiModel
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class PhotoUiMapperTest {

    private lateinit var photoUiMapper: PhotoUiMapper

    @Before
    fun setUp() {
        photoUiMapper = PhotoUiMapper()
    }

    @Test
    fun mapToView() {
        // Arrange
        val photo = PresentationPhotoFactory.generatePhoto()

        // Act
        val photoView = photoUiMapper.mapToView(photo)

        // Assert
        assertPhotoMapDataEqual(photoView, photo)
    }

    /**
     * Helpers Methods
     */
    private fun assertPhotoMapDataEqual(photoUiModel: PhotoUiModel, photo: PhotoDomainModel) {
        assertEquals(photoUiModel.thumbnailUrl, photo.thumbnailUrl)
        assertEquals(photoUiModel.id, photo.id)
        assertEquals(photoUiModel.albumId, photo.albumId)
        assertEquals(photoUiModel.url, photo.url)
    }
}
