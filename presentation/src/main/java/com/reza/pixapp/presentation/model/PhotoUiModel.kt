package com.reza.pixapp.presentation.model

import java.io.Serializable

data class PhotoUiModel(
    val albumId: Int,
    val id: Int,
    val thumbnailUrl: String,
    val title: String,
    val url: String
) : Serializable