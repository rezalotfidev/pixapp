package com.reza.pixapp.presentation.paging

import androidx.paging.PagingState
import androidx.paging.rxjava3.RxPagingSource
import com.reza.pixapp.domain.usecase.GetPhotoListUseCase
import com.reza.pixapp.presentation.mapper.PhotoUiMapper
import com.reza.pixapp.presentation.model.PhotoUiModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers

const val STARTING_PAGE_INDEX = 1


class PhotosPagingSource(
    private val getPhotoListUseCase: GetPhotoListUseCase,
    private val photoUiMapper: PhotoUiMapper,
    private val pageSize: Int
) : RxPagingSource<Int, PhotoUiModel>() {
    override fun getRefreshKey(state: PagingState<Int, PhotoUiModel>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

    override fun loadSingle(params: LoadParams<Int>): Single<LoadResult<Int, PhotoUiModel>> {
        val page = params.key ?: STARTING_PAGE_INDEX

        return getPhotoListUseCase.buildUseCaseObservable()
            .observeOn(Schedulers.io())
            .subscribeOn(AndroidSchedulers.mainThread())
            .map {
                val nexKey = if (it.count() < pageSize)
                    null
                else
                    page + 1

                LoadResult.Page<Int, PhotoUiModel>(
                    data = it.map { item -> photoUiMapper.mapToView(item) },
                    prevKey = if (page == STARTING_PAGE_INDEX) null else page,
                    nextKey = nexKey
                ) as LoadResult<Int, PhotoUiModel>
            }.onErrorReturn {
                LoadResult.Error<Int, PhotoUiModel>(it)
            }
    }
}