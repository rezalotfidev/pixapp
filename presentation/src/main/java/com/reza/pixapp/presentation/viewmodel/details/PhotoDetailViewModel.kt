package com.reza.pixapp.presentation.viewmodel.details

import androidx.lifecycle.MutableLiveData
import com.reza.pixapp.domain.model.PhotoDomainModel
import com.reza.pixapp.domain.usecase.GetPhotoDetailUseCase
import com.reza.pixapp.presentation.base.BaseViewModel
import com.reza.pixapp.presentation.mapper.PhotoUiMapper
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.observers.DisposableSingleObserver
import io.reactivex.rxjava3.schedulers.Schedulers

class PhotoDetailViewModel (
    private val photoUiMapper: PhotoUiMapper,
    private val getPhotoDetailUseCase: GetPhotoDetailUseCase,
) : BaseViewModel<PhotoDetailState>() {

    override val _stateObservable: MutableLiveData<PhotoDetailState> by lazy {
        MutableLiveData<PhotoDetailState>()
    }

    private var state: PhotoDetailState = PhotoDetailState.Init
        set(value) {
            field = value
            publishState(value)
        }

    fun fetchPhotoDetails(id: Int) {
        state = PhotoDetailState.Loading

        getPhotoDetailUseCase.buildUseCaseObservable(id)
            .subscribeOn(AndroidSchedulers.mainThread())
            .observeOn(Schedulers.io())
            .doOnSuccess {
                state = PhotoDetailState.PhotoListSuccess(photoUiMapper.mapToView(it))
            }
            .doOnError {
                state = PhotoDetailState.Error(it.localizedMessage)
            }
            .subscribe()
    }

}