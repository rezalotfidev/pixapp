package com.reza.pixapp.presentation.viewmodel.photos

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.rxjava3.cachedIn
import androidx.paging.rxjava3.observable
import com.reza.pixapp.domain.model.PhotoDomainModel
import com.reza.pixapp.domain.usecase.GetPhotoListUseCase
import com.reza.pixapp.presentation.base.BaseViewModel
import com.reza.pixapp.presentation.mapper.PhotoUiMapper
import com.reza.pixapp.presentation.paging.PhotosPagingSource
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.observers.DisposableSingleObserver
import io.reactivex.rxjava3.schedulers.Schedulers

class PhotosViewModel(
    private val photoUiMapper: PhotoUiMapper,
    private val getPhotoListUseCase: GetPhotoListUseCase,
) : BaseViewModel<PhotosState>() {

    override val _stateObservable: MutableLiveData<PhotosState> by lazy {
        MutableLiveData<PhotosState>()
    }

    private var state: PhotosState = PhotosState.Init
        set(value) {
            field = value
            publishState(value)
        }

    @Suppress("OPT_IN_USAGE")
    fun fetchPhotosList() {
        state = PhotosState.Loading
        addDisposable(Pager(
            config = PagingConfig(
                pageSize = 20,
                enablePlaceholders = false
            ),
            pagingSourceFactory = {
                PhotosPagingSource(
                    getPhotoListUseCase,
                    photoUiMapper,
                    pageSize = 20
                )
            }
        ).observable
            .cachedIn(viewModelScope)
            .doOnError {
                state = PhotosState.Error(it.localizedMessage)
            }
            .doOnNext {
                state = PhotosState.PhotoListSuccess(it)
            }
            .observeOn(Schedulers.io())
            .subscribeOn(AndroidSchedulers.mainThread())
            .subscribe()
        )
    }

}