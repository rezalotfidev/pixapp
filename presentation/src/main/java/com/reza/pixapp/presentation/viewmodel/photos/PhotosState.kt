package com.reza.pixapp.presentation.viewmodel.photos

import androidx.paging.PagingData
import com.reza.pixapp.presentation.model.PhotoUiModel

sealed class PhotosState {
    object Init : PhotosState()
    object Loading : PhotosState()
    data class Error(var message: String) : PhotosState()
    data class PhotoListSuccess(var listOfPhotoModels: PagingData<PhotoUiModel>) : PhotosState()
}