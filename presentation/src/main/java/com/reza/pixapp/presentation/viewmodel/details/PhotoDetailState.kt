package com.reza.pixapp.presentation.viewmodel.details

import com.reza.pixapp.presentation.model.PhotoUiModel

sealed class PhotoDetailState {
    object Init : PhotoDetailState()
    object Loading : PhotoDetailState()
    data class Error(var message: String) : PhotoDetailState()
    data class PhotoListSuccess(var photoUiModel: PhotoUiModel) : PhotoDetailState()
}