package com.reza.pixapp.presentation.mapper

import com.reza.pixapp.domain.model.PhotoDomainModel
import com.reza.pixapp.presentation.model.PhotoUiModel

class PhotoUiMapper {

    fun mapToView(model:PhotoDomainModel): PhotoUiModel {
        return PhotoUiModel(
            id = model.id,
            albumId = model.albumId,
            thumbnailUrl = model.thumbnailUrl,
            title = model.title,
            url = model.url
        )
    }

}
