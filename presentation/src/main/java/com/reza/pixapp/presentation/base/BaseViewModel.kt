package com.reza.pixapp.presentation.base

import androidx.annotation.CallSuper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable

abstract class BaseViewModel<T> : ViewModel() {
    private val disposables: CompositeDisposable = CompositeDisposable()

    protected abstract val _stateObservable: MutableLiveData<T>
    val stateObservable: LiveData<T> get() = _stateObservable

    protected open fun publishState(state: T) {
        _stateObservable.postValue(state)
    }

    protected fun addDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }

    private fun clearDisposables() {
        if (disposables.isDisposed.not())
            disposables.clear()
    }

    @CallSuper
    override fun onCleared() {
        super.onCleared()
        clearDisposables()
    }
}
